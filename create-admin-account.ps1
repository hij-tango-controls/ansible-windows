# Benutzerdaten
$Benutzername = "AdminHIJ"
$FullName = "Alex Kesslers HIJ-Admin"
$Beschreibung = "Administrator Account for Ansible Automation"
$SSHKeyPath = ".\AdminHIJ.pub"

function Get-ValidPassword {
    do {
        $Passwort1 = Read-Host "Bitte geben Sie Ihr Passwort ein" -AsSecureString
        $Passwort2 = Read-Host "Bitte geben Sie Ihr Passwort erneut ein" -AsSecureString

        $Ptr1 = [Runtime.InteropServices.Marshal]::SecureStringToBSTR($Passwort1)
        $Ptr2 = [Runtime.InteropServices.Marshal]::SecureStringToBSTR($Passwort2)

        try {
            $PlainPassword1 = [Runtime.InteropServices.Marshal]::PtrToStringAuto($Ptr1)
            $PlainPassword2 = [Runtime.InteropServices.Marshal]::PtrToStringAuto($Ptr2)

            if ($PlainPassword1 -eq $PlainPassword2) {
                Write-Host "Passwort bestätigt." -ForegroundColor Green
                return $Passwort1
            } else {
                Write-Host "Passwörter stimmen nicht überein. Bitte versuchen Sie es erneut." -ForegroundColor Red
            }
        }
        finally {
            [Runtime.InteropServices.Marshal]::ZeroFreeBSTR($Ptr1)
            [Runtime.InteropServices.Marshal]::ZeroFreeBSTR($Ptr2)
        }
    } while ($true)
}

# Passwort eingeben und validieren
$Passwort = Get-ValidPassword

# Benutzerkonto erstellen
$User = New-LocalUser -Name $Benutzername -Password $Passwort -FullName $FullName -Description $Beschreibung -AccountNeverExpires -UserMayNotChangePassword -PasswordNeverExpires

# Benutzer zur Administratorgruppe hinzufügen
Add-LocalGroupMember -Group "Administratoren" -Member $Benutzername

# SSH-Key aus Datei einlesen
$SSHKey = Get-Content -Path $SSHKeyPath -ErrorAction Stop

# SSH-Key konfigurieren
$UserHome = "C:\Users\$Benutzername"
$SSHDir = "$UserHome\.ssh"
$AuthorizedKeysPath = "$SSHDir\authorized_keys"

# Verzeichnis für SSH erstellen, falls nicht vorhanden
if (-Not (Test-Path $SSHDir)) {
    New-Item -ItemType Directory -Path $SSHDir -Force
}

# Überprüfen, ob die authorized_keys Datei existiert, und entsprechend handeln
if (Test-Path $AuthorizedKeysPath) {
    Add-Content -Path $AuthorizedKeysPath -Value $SSHKey
} else {
    New-Item -ItemType File -Path $AuthorizedKeysPath -Force
    Add-Content -Path $AuthorizedKeysPath -Value $SSHKey
}

# Berechtigungen für den SSH-Ordner und die authorized_keys Datei festlegen
icacls $SSHDir /inheritance:r
icacls $SSHDir /grant "$($Benutzername):`(OI`)`(CI`)F"
icacls $AuthorizedKeysPath /inheritance:r
icacls $AuthorizedKeysPath /grant "$Benutzername:F"

# Bestätigung
Write-Host "Benutzer $Benutzername wurde erstellt und der SSH-Schlüssel wurde konfiguriert."

